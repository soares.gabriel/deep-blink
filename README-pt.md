# Eye State Benchmark 👁️🧠

Classificar estado dos olhos (aberto ou fechado) utilizando [Eletroencefalografia (EEG)](https://en.wikipedia.org/wiki/Electroencephalography) como dado base aplicando [redes Long Short Term Memory (LSTM)](https://en.wikipedia.org/wiki/Long_short-term_memory) e variantes. Para fins de benchmarking, foi utilizado o [conjunto de dados de EEG](https://archive.ics.uci.edu/ml/datasets/EEG+Eye+State) com registro do estado do olho, disponível no repositório de aprendizado de máquina da Universidade da Califórnia em Irvine (UCI).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Gabriel Soares** - *Initial work* - [golf-sierra](https://github.com/golf-sierra)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
